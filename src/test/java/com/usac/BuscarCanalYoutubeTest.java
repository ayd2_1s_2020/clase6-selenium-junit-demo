/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

import com.usac.pages.BuscarCanalYoutube;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.assertEquals;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author Oscar
 */
public class BuscarCanalYoutubeTest {
    private WebDriver driver;
    BuscarCanalYoutube buscarcanal;
    
    @Before
    public void SetUpDriver() throws Exception {
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver(){
        driver.close();
        driver.quit();
    }
    
    @Given("^i open chrome explorer and navigate to \"([^\"]*)\"$")
    public void i_open_chrome_explorer_and_navigate_to(String arg1) throws Exception{        
        this.driver.navigate().to(arg1);
        buscarcanal = new BuscarCanalYoutube(driver);
        assertEquals("YouTube", driver.getTitle());
    }
    
    @When("^i search the channel \"([^\"]*)\"$")
    public void i_search_the_channel(String arg1) throws Exception {
        buscarcanal.RealizarBusqueda(arg1);
    }
    
    @Then("^i should be in the best Youtube Channel$")
    public void i_should_be_redirected_to_usac_site() throws Exception{
        assertEquals("Oscar Corleto - YouTube", driver.getTitle() );
    }
    
}
