package com.usac;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import com.usac.pages.YoutubeSearchVideo201314292;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchUsacAtoGoogleAndOpenYoutube201314292 {

    private WebDriver driver;
    YoutubeSearchVideo201314292 youtubeSearchPage201314292;
    @Before
    public void SetUpDriver() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\dellLaptop\\Downloads\\chromedriver.exe");
        this.driver = new ChromeDriver();
    }
    @After
    public void CloseDriver() {
        driver.close();
        driver.quit();
    }
    @Given("^i go the chrome and navigate to \"([^\"]*)\"$")
    public void i_go_the_chrome_and_navigate_to(String arg1) throws Exception {
        this.driver.navigate().to(arg1);
        youtubeSearchPage201314292 = new YoutubeSearchVideo201314292(driver);
        assertEquals("YouTube", driver.getTitle());
    }
    @When("^i search \"([^\"]*)\" and press enter$")
    public void i_search_and_press_enter(String arg1) throws Exception {
        youtubeSearchPage201314292.search(arg1);
    }

    @When("^i click on the video \"([^\"]*)\"$")
    public void i_click_on_the_link(String arg1) throws Exception {
        youtubeSearchPage201314292.clickOnResultWithText();
    }
    @Then("^i should play the video$")
    public void i_should_play_the_video() throws Exception {

        assertEquals("AyD2_Clase6", driver.getTitle());
    }

}
