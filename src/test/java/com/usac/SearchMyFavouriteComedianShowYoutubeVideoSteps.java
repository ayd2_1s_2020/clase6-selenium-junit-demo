package com.usac;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import com.usac.pages.GoogleSearchPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class SearchMyFavouriteComedianShowYoutubeVideoSteps {

    private WebDriver driver;
    GoogleSearchPage googleSearchPage;
    
    @Before
    public void SetUpDriver() throws Exception {
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver(){
        driver.close();
        driver.quit();
    }
    
    @Given("^i open the explorer and navigate to the page \"([^\"]*)\"$")
    public void i_open_the_explorer_and_navigate_to_the_page(String arg1) throws Exception{        
        this.driver.navigate().to(arg1);
        googleSearchPage = new GoogleSearchPage(driver);
        assertEquals("Google", driver.getTitle());
    }

    @When("^i search my favourite comedian show \"([^\"]*)\" and press enter$")
    public void i_search_my_favourite_comedian_show_and_press_enter(String arg1) throws Exception {
        googleSearchPage.search(arg1);
    }
    
    @When("^i click on the first link with text \"([^\"]*)\"$")
    public void i_click_on_the_first_link_with_text(String arg1) throws Exception {
        googleSearchPage.clickOnResultWithText(arg1);
    }

    @Then("^i should be redirected to the Youtube video$")
    public void i_should_be_redirected_to_the_Youtube_video() throws Exception{
        System.out.println(driver.getTitle());
        assertEquals("YouTube", driver.getTitle() );
    }


}