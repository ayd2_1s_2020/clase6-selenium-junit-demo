Feature: Write A Note At Notepad Page 201602822
    As user i want write a note

@SetupDriver
Scenario: 
    Given i open chrome explorer and navigate to "https://anotepad.com"
    When i write a note with title "201602822" and content "Prueba cucumber"
    And i click on save button
    Then i should have a new public note saved with title "201602822"