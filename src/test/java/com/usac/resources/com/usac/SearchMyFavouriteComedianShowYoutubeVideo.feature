Feature: Search My Favourite Comedian Show Youtube video
    As user i want to search Franco Escamilla RPM Completo to Youtube and open the first result

    @SetupDriver
    Scenario:
        Given i open the explorer and navigate to the page "http://www.google.com/"
        When i search my favourite comedian show "franco escamilla rpm completo" and press enter
        And i click on the first link with text "Franco Escamilla RPM Completo - YouTube"
        Then i should be redirected to the Youtube video