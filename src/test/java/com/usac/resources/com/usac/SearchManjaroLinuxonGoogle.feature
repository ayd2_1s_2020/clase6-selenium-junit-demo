Feature: Search Manjaro Linux on Google and open the First official site
    As user i want to search Manjaro Linux on Google and open the official site from result

@SetupDriver
Scenario: 
    Given i open firefox and navigate to "http://www.google.com"
    When i search "manjaro" and press enter
    And i click on the link with text "Manjaro - enjoy the simplicity"
    Then i should be redirected to Manjaro official site
    And i click on the download button with url "/download/"

