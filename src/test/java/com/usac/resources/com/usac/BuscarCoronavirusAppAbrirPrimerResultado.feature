Feature: Buscar la plataforma coronavirus.app en Google and abrir la plataforma
    Como usuario yo quiero buscar coronavirus.app y abrir su plataforma como resultado

@SetupDriver
Scenario: 
    Given Yo abro el explorador y me dirijo a "http://www.google.com"
    When yo busco la plataforma "coronavirus.app"
    And yo presiono la opcion que dice "The Coronavirus App"
    Then Yo debo ser redirigido a la plataforma The Coronavirus App