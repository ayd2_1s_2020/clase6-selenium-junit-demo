Feature: Busco el sitio de Nintendo oficial en google y abro el primer sitio
    Como usuario, quiero buscar la pagina de Nintendo y abrir About Nintendo

@SetupDriver
Scenario: 
    Given Abro el explorador y navego a "http://www.google.com"
    When busco "Nintendo - Official Site - Video Game Consoles, Games" y presiono enter
    And hago clic en el enlace con el texto "About Nintendo‎"
    Then debería ser redirigido al sitio de de acerca de Nintendo.

