Feature: Search Cassandra site at Google and open the official site
    As user i want to search Cassandra to Google and open the Cassandra site from result

@SetupDriver
Scenario: 
    Given i open chrome and navigate to "http://www.google.com"
    When i search "cassandra" and press intro
    And i click over the link with text "Apache Cassandra"
    Then i should be redirected to cassandra site

