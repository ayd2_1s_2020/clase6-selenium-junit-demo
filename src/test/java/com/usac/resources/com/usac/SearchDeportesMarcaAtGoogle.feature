Feature: Buscar la pagina de Marca en Google y abrir la pagina
    Como usuario yo quiero buscar Deportes Marca y 
    abrir su pagina como resultado para ver las ultimas noticias del deporte en medio del Coronavirus

@SetupDriver
Scenario: 
    Given Yo voy a la pagina "http://www.google.com"
    When yo busco "Marca Deportes"
    And yo presiono "MARCA - Diario online líder en información deportiva"
    Then Yo debo ser redirigido a la pagina de Marca