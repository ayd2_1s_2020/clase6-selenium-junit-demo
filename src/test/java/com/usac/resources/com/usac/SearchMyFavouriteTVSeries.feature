Feature: Search my favourite TV series
    As user i want to search Sabrina to google and open netflix result

@SetupDriver
Scenario: 
    Given i open the explorer and go to "http://www.google.com"
    When i search my favourite TV series "netflix sabrina" and press enter
    And i click on the netflix link with text "El mundo oculto de Sabrina | Sitio oficial de Netflix"
    Then i should be redirected to Netflix site

