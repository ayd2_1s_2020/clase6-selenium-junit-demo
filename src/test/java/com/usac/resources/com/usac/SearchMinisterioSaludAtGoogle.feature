Feature: Buscar la pagina del ministerio de salud guatemalteco en Google and abrir la pagina
    Como usuario yo quiero buscar ministerio de salud y 
    abrir su pagina como resultado para observar recomendaciones acerca covid-19

@SetupDriver
Scenario: 
    Given Yo abro el explorador y me dirijo a "http://www.google.com"
    When yo busco la plataforma "ministerio de salud"
    And yo presiono la opcion que dice "Ministerio de Salud Pública - Inicio"
    Then Yo debo ser redirigido a la pagina del ministerio de salud guatemalteco