Feature: Search Ruby site at Google and open the official site
    As user i want to search Ruby on Google and open the Ruby spanish site from result

@SetupDriver
Scenario: 
    Given i open chrome and navigate to "http://www.google.com"
    When i search "ruby" and press intro
    And i click over the link with text "Acerca de Ruby - Ruby Programming Language"
    Then i should be redirected to Ruby site