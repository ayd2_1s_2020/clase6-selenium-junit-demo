Feature: Buscar la pagina de classroom para iniciar sesion
    As usuario deseo registrarme para classroom

@SetupDriver
Scenario: 
    Given abro el explorador y navego a "http://www.google.com"
    When busco "classroom" 
    And presiono el link con el texto "Inicia sesión: Cuentas de Google - Classroom - Google"
    Then deberia de mostrarme la pagina inicial de classroom

