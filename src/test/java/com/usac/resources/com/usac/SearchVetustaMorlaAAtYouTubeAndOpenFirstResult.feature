Feature: Search Vetusta Morla video at YouTube and open the First video
    As user i want to search Vetusta Morla to YouTube and open the first video  from result

@SetupDriver
Scenario: 
    Given i open the explorer and navigate to the link "http://www.youtube.com"
    When i search in YouTube "Vetusta Morla - 23 de Junio" and press enter
    And i click on the link of the video with text "Vetusta Morla - 23 de Junio"
    Then i should be redirected to the video

