Feature: Busque el sitio de PolloCampero en Google y abra el primer sitio
    Como usuario, quiero buscar PolloCampero en Google y abrir el sitio Ordena Online En Pollo Campero - Combo 8 Piezas Por Q134‎  desde el resultado

@SetupDriver
Scenario: 
    Given Abro el explorador y navego a "http://www.google.com"
    When busco "PolloCampero" y presiono enter
    And hago clic en el enlace con el texto "Ordena Online En Pollo Campero - Combo 8 Piezas Por Q134‎"
    And realizo la accion "Agregar"
    Then debería ser redirigido al sitio de compra del combo

