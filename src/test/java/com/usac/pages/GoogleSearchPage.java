package com.usac.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GoogleSearchPage {
    private WebDriver driver;

    public GoogleSearchPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getSearchElement(){
        return driver.findElement(By.xpath("//input[@title='Buscar']"));
    }
    
    public void clickOnResultWithText (String text ){
        driver.findElement(By.xpath("//h3[text()='" + text + "']/..")).click();
    }

    public void search (String text){
        getSearchElement().sendKeys(text+ Keys.ENTER);
    }

}