package com.usac.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GoogleSearchlacuerda {
    private WebDriver webdriver;

    public GoogleSearchlacuerda(WebDriver webdriver) {
        
        this.webdriver = webdriver;
    }

    public WebElement getSearch() {
        //en firefox al realizar el ctrl + shift + k
        //podemos ver que existe un input que es el que
        //realiza la busqueda al darle click
        return webdriver.findElement(By.xpath("//input[@title='Buscar']"));
    }

    public void clickManjaroOfficial(String firstTextFound) {
        //buscamos aquella etiqueta H3 con el nombre del primer
        //resultado obtenido al buscar manjaro, en este caso se
        //espera que sea la pagina oficial y damos click
        webdriver.findElement(By.xpath("//h3[text()='" + firstTextFound + "']/..")).click();
    }

    public void clickManjaroDownload(String url) {
        //buscamos el html a que redirecciona a la
        //pagina de descarga de manjaro linux 
        webdriver.findElement(By.xpath("//a[@href='" + url + "']/..")).click();
    }

    public void SearchManjaro(String ManjaroText) {
        //obtenemos el componente HTML y realizamos
        //la busqueda de manjaro y finalizamos dando enter
        getSearch().sendKeys( ManjaroText + Keys.ENTER);
    }

}