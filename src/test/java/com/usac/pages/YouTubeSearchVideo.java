package com.usac.pages;
//import org.openqa.selenium.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;
public class YouTubeSearchVideo {
    private WebDriver driver;
    //WebDriverWait wait = new WebDriverWait(driver, 1000);
    public YouTubeSearchVideo(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS); 
    }

    public WebElement getSearchElement(){
        return driver.findElement(By.xpath("//input[@id='search']"));
       // return driver.findElement(By.findElement())
    }
    ////body/ytd-app/div[@id='content']/ytd-page-manager[@id='page-manager']/ytd-search[@class='style-scope ytd-page-manager']/div[@id='container']/ytd-two-column-search-results-renderer[@class='style-scope ytd-search']/div[@id='primary']/ytd-section-list-renderer[@class='style-scope ytd-two-column-search-results-renderer']/div[@id='contents']/ytd-item-section-renderer[@class='style-scope ytd-section-list-renderer']/div[@id='contents']/ytd-video-renderer[1]/div[1]/div[1]/div[1]/div[1]/h3[1]/a[1]
    public void clickOnResultWithText (String text ){
        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//yt-formatted-string[contains(text(),'Vetusta Morla - 23 de Junio')]")));
        driver.findElement(By.xpath("//yt-formatted-string[contains(text(),'Vetusta Morla - 23 de Junio')]")).click();
    }

    public void search (String text){
        getSearchElement().sendKeys(text+ Keys.ENTER);
    }

}

///html[1]/body[1]/ytd-app[1]/div[1]/ytd-page-manager[1]/ytd-search[1]/div[1]/ytd-two-column-search-results-renderer[1]/div[1]/ytd-section-list-renderer[1]/div[2]/ytd-item-section-renderer[1]/div[3]/ytd-video-renderer[1]/div[1]/div[1]
///html[1]/body[1]/ytd-app[1]/div[1]/ytd-page-manager[1]/ytd-search[1]/div[1]/ytd-two-column-search-results-renderer[1]/div[1]/ytd-section-list-renderer[1]/div[2]/ytd-item-section-renderer[1]/div[3]/ytd-video-renderer[1]/div[1]/div[1]/div[1]/div[1]/h3[1]/a[1]/yt-formatted-string[1]
///html[1]/body[1]/ytd-app[1]/div[1]/ytd-page-manager[1]/ytd-search[1]/div[1]/ytd-two-column-search-results-renderer[1]/div[1]/ytd-section-list-renderer[1]/div[2]/ytd-item-section-renderer[1]/div[3]/ytd-video-renderer[1]/div[1]/div[1]/div[1]/div[1]/h3[1]/a[1]
///html/body/ytd-app/div/ytd-page-manager/ytd-search/div[1]/ytd-two-column-search-results-renderer/div/ytd-section-list-renderer/div[2]/ytd-item-section-renderer/div[3]/ytd-shelf-renderer[1]/div[1]/div[2]/ytd-vertical-list-renderer/div[1]/ytd-video-renderer[1]/div[1]/div/div[1]/div/h3
////*[@id="thumbnail"]/yt-img-shadow
//*[@id="video-title"]/yt-formatted-string