package com.usac.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NotepadPage {
    private WebDriver driver;
    private WebDriverWait wait;

    public NotepadPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 100);   
    }

    public WebElement getNoteTitleElement(){
        return driver.findElement(By.id("edit_title"));//xpath("//input[@id='edit_title']"));
    }

    public WebElement getNoteContentElement(){
        return driver.findElement(By.id("edit_textarea"));
    }

    public void clickOnEnableRichText (){
        driver.findElement(By.id("btnEnableRichText")).click();
    }

    public void clickOnSaveNoteButton (){
        driver.findElement(By.id("btnSaveNote")).click();
    }

    public void writeNote (String title, String content){
        getNoteTitleElement().sendKeys(title);
        getNoteContentElement().sendKeys(content);
    }

    public String getTitleOfNewPublicNote(){
        String xp = "/html/body/div[2]/div/div[3]/div[2]/div[2]/div[2]/div[2]/ul/li[1]/a[2]";
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xp)));
        WebElement note = driver.findElement(By.xpath(xp));
        return note.getText();
    }

}