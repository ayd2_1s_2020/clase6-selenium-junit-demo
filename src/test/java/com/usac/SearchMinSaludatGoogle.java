package com.usac;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import com.usac.pages.GoogleSearchPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class SearchMinSaludatGoogle {

    private WebDriver driver;
    GoogleSearchPage googleSearchPage;
    
    @Before
    public void SetUpDriver() throws Exception {
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver(){
        driver.close();
        driver.quit();
    }
    
    @Given("^Yo abro el explorador y me dirijo a \"([^\"]*)\"$")
    public void Yo_abro_el_explorador_y_me_dirijo_a(String arg1) throws Exception{        
        this.driver.navigate().to(arg1);
        googleSearchPage = new GoogleSearchPage(driver);
        assertEquals("Google", driver.getTitle());
    }

    @When("^yo busco la plataforma \"([^\"]*)\"$")
    public void yo_busco_la_plataforma(String arg1) throws Exception {
        googleSearchPage.search(arg1);
    }
    
    @When("^yo presiono la opcion que dice \"([^\"]*)\"$")
    public void i_click_on_the_link_with_text(String arg1) throws Exception {
        googleSearchPage.clickOnResultWithText(arg1);
    }

    @Then("^Yo debo ser redirigido a la plataforma Ministerio de Salud Publica$")
    public void Yo_debo_ser_redirigido_a_la_plataforma() throws Exception{
        assertEquals("Ministerio de Salud Pública - Inicio", driver.getTitle() );
    }
}