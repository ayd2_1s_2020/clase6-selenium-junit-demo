package com.usac;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import com.usac.pages.NotepadPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Write A Note At Notepad Page
 */
public class WriteANoteAtNotepadPage201602822 {

    private WebDriver driver;
    NotepadPage notepadPage;
    
    @Before
    public void SetUpDriver() throws Exception {
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver(){
        driver.close();
        driver.quit();
    }
    
    @Given("^i open chrome explorer and navigate to \"([^\"]*)\"$")
    public void i_open_chrome_explorer_and_navigate_to(String arg1) throws Exception{        
        this.driver.navigate().to(arg1);
        notepadPage = new NotepadPage(driver);
        assertEquals("Online Notepad - free at Anotepad.com", driver.getTitle());
    }

    @When("^i write a note with title \"([^\"]*)\" and content \"([^\"]*)\"$")
    public void i_search_and_press_enter(String arg1, String arg2) throws Exception {
        notepadPage.writeNote(arg1, arg2);
    }
    
    @When("^i click on save button$")
    public void i_click_on_save_button() throws Exception {
        notepadPage.clickOnSaveNoteButton();
    }

    @Then("^i should have a new public note saved with title \"([^\"]*)\"$")
    public void i_should_have_a_new_public_note_saved_with_title(String arg1) throws Exception{
        assertEquals(arg1, notepadPage.getTitleOfNewPublicNote());
    }
}