package com.usac;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import com.usac.pages.GoogleSearchPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


/**
 * SearchhFigmaEdelgard
 */
public class SearchFigmaEdelgard {

    private WebDriver driver;
    GoogleSearchPage googleSearchPage;
    
    @Before
    public void SetUpDriver() throws Exception {
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver(){
        driver.close();
        driver.quit();
    }
    
    @Given("^i open the explorer and navigate to \"([^\"]*)\"$")
    public void i_open_the_explorer_and_navigate_to(String arg1) throws Exception{        
        this.driver.navigate().to(arg1);
        googleSearchPage = new GoogleSearchPage(driver);
        assertEquals("Google", driver.getTitle());
    }

    @When("^i search \"([^\"]*)\" and press enter$")
    public void i_search_and_press_enter(String arg1) throws Exception {
        googleSearchPage.search(arg1);
    }
    
    @When("^i click on the link with text \"([^\"]*)\"$")
    public void i_click_on_the_link_with_text(String arg1) throws Exception {
        googleSearchPage.clickOnResultWithText(arg1);
    }

    @Then("^i should be redirected to goodsmile site$")
    public void i_should_be_redirected_to_goodsmile_site() throws Exception{
        assertEquals("figma Edelgard von Hresvelg", driver.getTitle() );
    }

}