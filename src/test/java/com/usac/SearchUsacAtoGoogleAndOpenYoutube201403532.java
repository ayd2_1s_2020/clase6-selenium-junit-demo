/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

/**
 *
 * @author chicas
 */
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import com.usac.pages.YoutubeSearchVideo20140353;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchUsacAtoGoogleAndOpenYoutube201403532 {

    private WebDriver driver;
    YoutubeSearchVideo20140353 youtubeSearchPage201403532;

    @Before
    public void SetUpDriver() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\chicas\\Downloads\\chromedriver.exe");
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver() {
        driver.close();
        driver.quit();
    }

    @Given("^i go the explorer and navigate to \"([^\"]*)\"$")
    public void i_open_the_explorer_and_navigate_to(String arg1) throws Exception {
        this.driver.navigate().to(arg1);
        youtubeSearchPage201403532 = new YoutubeSearchVideo20140353(driver);
        assertEquals("YouTube", driver.getTitle());
    }

    @When("^i search in youtube \"([^\"]*)\" and press enter$")
    public void i_search_and_press_enter(String arg1) throws Exception {
        youtubeSearchPage201403532.search(arg1);
    }

    @When("^i click on the video link with text \"([^\"]*)\"$")
    public void i_click_on_the_link_with_text(String arg1) throws Exception {
        youtubeSearchPage201403532.clickOnResultWithText();
    }

    @Then("^i should play de video$")
    public void i_should_be_redirected_to_usac_site() throws Exception {

        assertEquals("La Capital - YouTube", driver.getTitle());
    }

}
