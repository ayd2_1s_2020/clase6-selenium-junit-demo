package com.usac;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import com.usac.pages.GoogleSearchPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
public class T5_201314646_Busqueda {

    private WebDriver driver;
    GoogleSearchPage googleSearchPage;
    
    @Before
    public void SetUpDriver() throws Exception {
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver(){
        driver.close();
        driver.quit();
    }

    @Given("^Abro el explorador y navego a \"([^\"]*)\"$")
    public void Abro_el_explorador_y_navego_a(String arg1) throws Exception{        
        this.driver.navigate().to(arg1);
        googleSearchPage = new GoogleSearchPage(driver);
        assertEquals("Google", driver.getTitle());
    }

    @When("^busco \"([^\"]*)\" y presiono enter$")
    public void busco_y_presiono_enter(String arg1) throws Exception {
        googleSearchPage.search(arg1);
    }
    
    @When("^hago clic en el enlace con el texto \"([^\"]*)\"$")
    public void hago_clic_en_el_enlace_con_el_texto(String arg1) throws Exception {
        googleSearchPage.clickOnResultWithText(arg1);
    }

    @When("^realizo la accion \"([^\"]*)\"$")
    public void realizo_la_accion(String arg2) throws Exception {
        googleSearchPage.clickOnResultWithText(arg2);
    }

    @Then("^debería ser redirigido al sitio de compra del combo$")
    public void redirige_al_sitio_de_compra() throws Exception{
        assertEquals("Ordena Online En Pollo Campero - Combo 8 Piezas Por Q134‎", driver.getTitle() );
    }
}