package com.usac;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import com.usac.pages.YoutubeSearchPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


/**
 * SearchFavoriteVideoAtYoutube
 */
public class SearchFavoriteVideoAtYoutube {

    private WebDriver driver;
    YoutubeSearchPage youtubeSearchPage;
    
    @Before
    public void SetUpDriver() throws Exception {
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver(){
        driver.close();
        driver.quit();
    }
    
    @Given("^i go the explorer and navigate to \"([^\"]*)\"$")
    public void i_open_the_explorer_and_navigate_to(String arg1) throws Exception{        
        this.driver.navigate().to(arg1);
        youtubeSearchPage = new YoutubeSearchPage(driver);
        assertEquals("YouTube", driver.getTitle());
    }
    @When("^i search in youtube \"([^\"]*)\" and press enter$")
    public void i_search_and_press_enter(String arg1) throws Exception {
        youtubeSearchPage.search(arg1);
    }
    
    @When("^i click on the video link with text \"([^\"]*)\"$")
    public void i_click_on_the_link_with_text(String arg1) throws Exception {
        youtubeSearchPage.clickOnResultWithText();
    }
    
    @Then("^i should play de video$")
    public void i_should_be_redirected_to_usac_site() throws Exception{
        
   
        assertEquals("Dross - YouTube", driver.getTitle() );
    }
}