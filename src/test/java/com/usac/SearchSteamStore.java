package com.usac;

import com.usac.pages.*;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
public class SearchSteamStore {

    private WebDriver driver;
    GoogleSearchPage googleSearchPage;
    
    @Before
    public void SetUpDriver() throws Exception {
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver(){
        driver.close();
        driver.quit();
    }

    @Given("^Abro navegador y me voy a \"([^\"]*)\"$")
    public void Abro_navegador_y_me_voy_a(String arg1) throws Exception{        
        this.driver.navigate().to(arg1);
        googleSearchPage = new GoogleSearchPage(driver);
        assertEquals("Google", driver.getTitle());
    }

    @When("^escribo \"([^\"]*)\" y presiono Enter$")
    public void Escribo_y_presiono_Enter(String arg1) throws Exception {
        googleSearchPage.search(arg1);
    }
    
    @When("^Hago click en \"([^\"]*)\"$")
    public void hago_clic_en(String arg1) throws Exception {
        googleSearchPage.clickOnResultWithText(arg1);
    }

    @Then("^me redirigo al sitio de juegos de steam$")
    public void me_redirigo_al_sitio_de_juegos_de_steam() throws Exception{
        assertEquals("Juegos en Steam", driver.getTitle() );
    }
}