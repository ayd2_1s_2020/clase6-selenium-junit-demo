package com.usac;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import com.usac.pages.YouTubeSearchVideo;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


/**
 * SearchVetustaMorlaAAtYouTubeAndOpenFirstResult
 */
public class SearchVetustaMorlaAAtYouTubeAndOpenFirstResult {

    private WebDriver driver;
    YouTubeSearchVideo youtubesearch;
    
    @Before
    public void SetUpDriver() throws Exception {
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver(){
        driver.close();
        driver.quit();
    }
    
    @Given("^i open the explorer and navigate to the link \"([^\"]*)\"$")
    public void i_open_the_explorer_and_navigate_to(String arg1) throws Exception{        
        this.driver.navigate().to(arg1);
        youtubesearch = new YouTubeSearchVideo(driver);
        //driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS); 
        assertEquals("YouTube", driver.getTitle());
    }

    @When("^i search in YouTube \"([^\"]*)\" and press enter$")
    public void i_search_and_press_enter(String arg1) throws Exception {
        youtubesearch.search(arg1);
    }
    
    @When("^i click on the link of the video with text \"([^\"]*)\"$")
    public void i_click_on_the_link_with_text(String arg1) throws Exception {
        youtubesearch.clickOnResultWithText(arg1);
    }

    @Then("^i should be redirected to the video$")
    public void i_should_be_redirected_to_usac_site() throws Exception{
        assertEquals("Vetusta Morla - 23 de Junio - YouTube", driver.getTitle() );
    }

}