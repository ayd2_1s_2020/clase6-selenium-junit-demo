package com.usac;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.assertEquals;
import com.usac.pages.GoogleSearchPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


/**
 * SearchUsacAtGoogleAndOpenFirstResultSteps
 */
public class SearchUsacAtGoogleAndOpenFirstResultSteps {

    private WebDriver driver;
    GoogleSearchPage googleSearchPage;
    
    @Before
    public void SetUpDriver() throws Exception {
        this.driver = new ChromeDriver();
    }

    @After
    public void CloseDriver(){
        driver.close();
        driver.quit();
    }
    
    @Given("^i open the explorer and navigate to \"([^\"]*)\"$")
    public void abro_el_explorador_y_navego_a(String arg1) throws Exception{        
        this.driver.navigate().to(arg1);
        googleSearchPage = new GoogleSearchPage(driver);
        assertEquals("Google", driver.getTitle());
    }

    @When("^i search \"([^\"]*)\" and press enter$")
    public void busco(String arg1) throws Exception {
        googleSearchPage.search(arg1);
    }
    
    @When("^i click on the link with text \"([^\"]*)\"$")
    public void presiono_el_link_con_el_texto(String arg1) throws Exception {
        googleSearchPage.clickOnResultWithText(arg1);
    }

    @Then("^i should be redirected to cucumber site$")
    public void deberia_de_mostrarme_la_pagina_inicial_de_classroom() throws Exception{
        assertEquals("Clases", driver.getTitle() );
    }
}