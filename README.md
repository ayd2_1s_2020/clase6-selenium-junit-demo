# clase6-selenium-junit-demo

Demo project for Selenium Tests with JUnit and Cucumber
This project is intended to be a How-to guide to get started to test automation with Selenium using Java, JUnit and Cucumber.

What does this project do?
- Runs an automated test on Chrome with JUnit runner. 

For futher documentation please refer to [How-to Get started with Selenium using JUnit and Cucumber](under construction)

## Getting Started

[Fork](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork) this repository using the gitlab mechanism or you can clone the entire repository as follows:

```bash
git clone git@gitlab.com:ayd2_1s_2020/clase6-selenium-junit-demo.git
```
```bash
git clone https://gitlab.com/ayd2_1s_2020/clase6-selenium-junit-demo.git
```

### Tools

The main tools used in this pipeline are:

- Selenium: A testing framework that provides tools to automate acceptance tests for web applications by test scripts that can be developed in multiple languages for multiple browsers on all major OS. 
- Cucumber: A tool that supports BDD and help people to automate scenarios or acceptance tests that describes the behavior of the system from the customer's perspective.
- Java 8: General purpose, multiplatform programming language. 
- JUnit 4: Unit testing framework form the Java programming language.
- Maven: Build automation tool for java projects.


Those tools will be applied in the areas:
- Test Automation

### Prerequisites

To be able to run this example project you should download and install the following tools 

| Name | Version | Download Link | 
| --- | --- | --- |
| Java SE Development Kit | 8u231 | https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html?ssSourceSiteId=otnes |
| Maven | 3.6.2| https://maven.apache.org/download.cgi|
| Chrome | 78 | https://www.google.com/chrome/ |
| Chrome Driver | 78 | https://chromedriver.chromium.org/downloads |

You need to add Chrome Driver to your PATH variable

### Run Selenium Test

To run the example escanario you need to download and compile the project the libraries by using command
```bash
  mvn compile
```

Then you can execute 
```bash
  mvn test
```

## Author

* **Dodany Girón**      - dodanygiron@gmail.com 
Análisis y DIseño de sistemas 2
USAC
